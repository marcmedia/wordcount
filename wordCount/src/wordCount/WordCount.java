package wordCount;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Scanner;

public class WordCount {

	public static void main(String[] args) throws IOException {
		
		AllWords allWords = new AllWords();
		
		Scanner scanner = new Scanner(new File("faust-teil-eins.txt"), StandardCharsets.UTF_8);
		
		scanner.useDelimiter("[ ,\\.()\\n\\t\\r\\!\\?:;\"]+");
		scanner.delimiter();
		
		while (scanner.hasNext()) {
			String word = scanner.next();
			allWords.add(word);
			
		}
		
		
		System.out.println("Anzahl aller Wörter im Text: " + allWords.totalWords());
		System.out.println("Anzahl Vokabulargröße: " + allWords.distinctWords());
		System.out.println("------------------------------------");
		System.out.println(allWords.toString());
		
	}

}
