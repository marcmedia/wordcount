package wordCount;

public class Word implements Comparable<Word> {
	
	private String content; // das Wort als Zeichenkette
	private int numberOfContains; // die Anzahl des Auftretens dieses Wortes im Text
	
	public Word(String content) {
		this.content = content;
		this.numberOfContains = 1;
	}
	
	public int count() { 
		return this.numberOfContains; 
	}

	
	public void inc() { 
		++this.numberOfContains;
	}
	
	
	public String getContent() {
		return this.content;
	}

	
	public String toString(){
		return this.getContent() + " : " + this.count() + "\n";
	}

	@Override
	public int compareTo(Word word) {
		return this.content.compareTo(word.getContent());
	}
}